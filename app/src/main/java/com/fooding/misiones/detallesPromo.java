package com.fooding.misiones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fooding.misiones.Adaptador.ProductosAdaptador;
import com.fooding.misiones.Model.Producto;
import com.fooding.misiones.Model.Promos;
import com.fooding.misiones.RoomDB.MainData;
import com.fooding.misiones.RoomDB.TablaCarrito;
import com.fooding.misiones.RoomDB.UserDB;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class detallesPromo extends AppCompatActivity {
    Promos promos;
    private static String nombre_promo = "";
    public static final String extra_Promo = "extra_Promo";
    private ArrayAdapter<String> adaptador;
    private RecyclerView lista = null;
    private ProductosAdaptador adapter;
    private RecyclerView.LayoutManager layoutManager;
    private int click;
    UserDB database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_promo);

        database = UserDB.getInstance(this);
        promos = getIntent().getParcelableExtra(extra_Promo);

        ArrayList<Producto> productos = new ArrayList<>();
        ArrayList<Promos> mostrarProductos = new ArrayList<>();

        int sizeProductos = promos.getProductos().size();

        String nombreProducto = null;
        String descripcionProducto = null;

        for (int i = 0; i < sizeProductos ; i++) {
            nombreProducto = promos.getProductos().get(i).getNombre();
            descripcionProducto = promos.getProductos().get(i).getDescripcion();

            Log.d("[descripcionPromo]", descripcionProducto);
            productos.add(new Producto(nombreProducto,descripcionProducto));

        }
        //Log.d("[detallesPromo]", promos);



        ImageView imageView = findViewById(R.id.detallesImgPromo);
        TextView nombre = findViewById(R.id.detallesNombrePromo);
        TextView valor = findViewById(R.id.detallesValorPromo);
        TextView contador = findViewById(R.id.contador);
        Button plus = findViewById(R.id.plus);
        Button minus = findViewById(R.id.minus);
       Button agregar = findViewById(R.id.agregarCarrito);
       Button carrito = findViewById(R.id.goToCarrito);
       Button volver = findViewById(R.id.volverHome);

       carrito.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(detallesPromo.this, Carrito.class);
               startActivity(intent);
           }
       });

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(detallesPromo.this, home.class);
                startActivity(intent);
            }
        });


        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contador.setText(String.valueOf(--click));

                if (click <= 1) {
                    click = 1;
                    contador.setText(String.valueOf(click));
                }
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click = click ;
                contador.setText(String.valueOf(++click));
            }
        });

        lista = findViewById(R.id.productos);
        lista.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        lista.setLayoutManager(layoutManager);

        adapter = new ProductosAdaptador(this, productos);
        lista.setAdapter(adapter);






        Picasso.Builder builder = new Picasso.Builder(this);
        builder.build().load(promos.getImgPromo())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(imageView);

        nombre.setText(promos.getNombre_promo());
        valor.setText(promos.getValor());


      agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombrePromo = nombre.getText().toString();
                String valorPromo = valor.getText().toString();
                String contadorPromo = contador.getText().toString();
                String idMongo = promos.get_id();
                int parseContador = Integer.parseInt(contadorPromo);
                int parseValor = Integer.parseInt(valorPromo) * parseContador;
                String imagenPromo = promos.getImgPromo();

                Log.d("DETALLES", imagenPromo);
                TablaCarrito data = new TablaCarrito();
                data.setObjectId(idMongo);
                data.setNombre_promo(nombrePromo);
                data.setValor(Integer.parseInt(valorPromo));
                data.setCantidad(parseContador);
                data.setImgPromo(imagenPromo);
                data.setPrecioIndividual(parseValor);
                database.userDao().insertCarrito(data);

                FancyToast.makeText(getApplicationContext(),"Producto agregado al carrito exitosamente!",FancyToast.LENGTH_LONG,FancyToast.SUCCESS, R.drawable.baseline_shopping_cart_white_24dp,false).show();
                Intent intent = new Intent(detallesPromo.this, home.class);
                startActivity(intent);
            }
        });
    }

}