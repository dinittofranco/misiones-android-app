package com.fooding.misiones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fooding.misiones.RoomDB.MainData;
import com.fooding.misiones.RoomDB.UserDB;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ConfirmarDireccion extends AppCompatActivity {
    EditText direccion;
    Button confirmar;
    private FirebaseAuth mAuth;
    List<MainData> dataEmail = new ArrayList<>();
    UserDB database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_direccion);
        mAuth = FirebaseAuth.getInstance();
        String currentEmail = Objects.requireNonNull(mAuth.getCurrentUser()).getEmail();

        database = UserDB.getInstance(this);

        dataEmail = database.userDao().getUserEmail(currentEmail);
        direccion = (EditText) findViewById(R.id.confirmarDireccion);
        confirmar = (Button) findViewById(R.id.btnDireccion);

        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(ConfirmarDireccion.this);
        if (signInAccount != null) {
            if(dataEmail.get(0).getDireccion().equals("-")) {
                FancyToast.makeText(getApplicationContext(),"Por favor, ingrese una direccion",FancyToast.LENGTH_LONG,FancyToast.WARNING, R.drawable.baseline_payments_white_24dp,false).show();

            } else {
                // Direccion cuenta Google
                direccion.setText(dataEmail.get(0).getDireccion());
            }
        } else {
            // Direccion cuenta Firebase
            direccion.setText(dataEmail.get(0).getDireccion());
        }

        confirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Inserto direccion al campo de usuario de google
                String direccionDB =  direccion.getText().toString();
                int elementId = dataEmail.get(0).getId();

                database.userDao().updateDireccion(elementId, direccionDB);
                Intent intent = new Intent(ConfirmarDireccion.this, MetodoPago.class);
                startActivity(intent);
            }
        });



    }
}