package com.fooding.misiones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fooding.misiones.Adaptador.CategoriasAdaptador;
import com.fooding.misiones.Adaptador.ProductosActivosAdaptador;
import com.fooding.misiones.Adaptador.PromoAdaptador;
import com.fooding.misiones.Model.Categorias;
import com.fooding.misiones.Model.Promos;
import com.fooding.misiones.RoomDB.MainData;
import com.fooding.misiones.RoomDB.UserDB;
import com.fooding.misiones.Servicios.MisionesClient;
import com.fooding.misiones.Servicios.MisionesServices;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class home extends AppCompatActivity {
    private FirebaseAuth mAuth;

    ProgressDialog progressDialog;
    private PromoAdaptador promoAdaptador;
    private ProductosActivosAdaptador productoAdaptador;
    private CategoriasAdaptador categoriasAdapter;
    private String nombre = "";
    private RecyclerView recyclerViewPromos;
    private RecyclerView recyclerViewProductos;
    private RecyclerView recyclerViewCategorias;
    TextView nombreCuenta;
    Button signOut, viajarCarrito;

    List<MainData> dataEmail = new ArrayList<>();
    UserDB database;

    GoogleSignInClient mGoogleSignInClient;


    List<Promos> promocionesList;
    List<Promos> productosList;
    List<Categorias> categoriasList;

    MisionesServices misionesServices;
    MisionesClient misionesClient;

    List<MainData> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        mAuth = FirebaseAuth.getInstance();
        String currentEmail = mAuth.getCurrentUser().getEmail();

        database = UserDB.getInstance(this);

        dataEmail = database.userDao().getUserEmail(currentEmail);


        dataList = database.userDao().getAllUsuarios();

        nombreCuenta = (TextView) findViewById(R.id.nombreCuenta);
        signOut = (Button) findViewById(R.id.salir);
        viajarCarrito = (Button) findViewById(R.id.carrito);

viajarCarrito.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(home.this, Carrito.class);
        startActivity(intent);
    }
});


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Obtengo la instancia GoogleSignInClient
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);



       GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (signInAccount != null) {
            String personName = signInAccount.getDisplayName();
            String personEmail = signInAccount.getEmail();
            String personId = signInAccount.getId();
            Uri personPhoto = signInAccount.getPhotoUrl();

            MainData data = new MainData();

            data.setNombre(personName);
            data.setDireccion("-");
            data.setEmail(personEmail);
            data.setIdGoogle(personId);
            database.userDao().insertUsuario(data);
            nombreCuenta.setText(personName+"!");


        } else if (dataEmail.size() > 0){
            nombreCuenta.setText(dataEmail.get(0).getNombre());
        }

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == R.id.salir) {
                    mAuth.signOut();
                    mGoogleSignInClient.signOut();
                    nombreCuenta.setText("");
                    if (signInAccount != null){
                        database.userDao().resetUsuario(signInAccount.getId());
                    }
                    database.userDao().resetCarrito();
                    startActivity(new Intent(home.this, loginPage.class));
                    finish();
                }
            }
        });



        retrofitInit();
        generateList();



    }

    private void retrofitInit() {
        misionesClient = MisionesClient.getInstance();
        misionesServices = misionesClient.getMisionesServices();
    }

    // FRONT END
    private void generateList() {

        Call<List<Categorias>> callCategorias = misionesServices.getCategorias();

        callCategorias.enqueue(new Callback<List<Categorias>>() {
            @Override
            public void onResponse(Call<List<Categorias>> call, Response<List<Categorias>> response) {
                if(response.isSuccessful()) {
                    categoriasList = response.body();
                    categoriasAdapter = new CategoriasAdaptador(home.this, categoriasList);
                    recyclerViewCategorias.setAdapter(categoriasAdapter);
                } else {
                    Toast.makeText(home.this, "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Categorias>> call, Throwable t) {
                Toast.makeText(home.this, "Error de conexion", Toast.LENGTH_SHORT).show();

            }
        });

        recyclerViewCategorias = findViewById(R.id.listaCategorias);
        recyclerViewCategorias.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManagerCategorias = new LinearLayoutManager(this);
        linearLayoutManagerCategorias.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewCategorias.setLayoutManager(linearLayoutManagerCategorias);

        Call<List<Promos>> call = misionesServices.getPromosActivas();
        call.enqueue(new Callback<List<Promos>>() {
            @Override
            public void onResponse(Call<List<Promos>> call, Response<List<Promos>> response) {
                if(response.isSuccessful()) {
                    promocionesList = response.body();
                    promoAdaptador = new PromoAdaptador(home.this, promocionesList);
                    recyclerViewPromos.setAdapter(promoAdaptador);

                } else {
                    Toast.makeText(home.this, "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Promos>> call, Throwable t) {
                Toast.makeText(home.this, "Error de conexion", Toast.LENGTH_SHORT).show();


            }
        });

        recyclerViewPromos = findViewById(R.id.listaPromos);
        recyclerViewPromos.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManagerPromo = new LinearLayoutManager(this);
        linearLayoutManagerPromo.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewPromos.setLayoutManager(linearLayoutManagerPromo);

        Call<List<Promos>> callProductos = misionesServices.getProductosActivos();

        callProductos.enqueue(new Callback<List<Promos>>() {
            @Override
            public void onResponse(Call<List<Promos>> call, Response<List<Promos>> response) {
                if(response.isSuccessful()) {
                    productosList = response.body();
                    Log.d("PRODUCTOS", productosList.toString());
                    productoAdaptador = new ProductosActivosAdaptador(home.this, productosList);
                    recyclerViewProductos.setAdapter(productoAdaptador);

                } else {
                    Toast.makeText(home.this, "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Promos>> call, Throwable t) {
                Toast.makeText(home.this, "Error de conexion", Toast.LENGTH_SHORT).show();


            }
        });

        recyclerViewProductos = findViewById(R.id.listaProductos);
        recyclerViewProductos.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManagerProductos = new LinearLayoutManager(this);
        linearLayoutManagerProductos.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewProductos.setLayoutManager(linearLayoutManagerProductos);
    }

}