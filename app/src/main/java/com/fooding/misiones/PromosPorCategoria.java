package com.fooding.misiones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fooding.misiones.Adaptador.PromoAdaptador;
import com.fooding.misiones.Model.Categorias;
import com.fooding.misiones.Model.Promos;
import com.fooding.misiones.Servicios.MisionesClient;
import com.fooding.misiones.Servicios.MisionesServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromosPorCategoria extends AppCompatActivity {

    MisionesServices misionesServices;
    MisionesClient misionesClient;

    // Objeto categorias de tipo categorias
    Categorias categorias;
    // Datos desde CategoriasAdaptador, posicion del item clickeado
    public static final String extra_Categoria = "extra_Categoria";

    private RecyclerView recyclerViewPromos;
    private PromoAdaptador promoAdaptador;
    List<Promos> promocionesList;

    // Vistas
    private TextView nombreCategoria;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promos_categorizadas);

        Button volver = (Button) findViewById(R.id.volver);

        categorias = getIntent().getParcelableExtra(extra_Categoria);
        nombreCategoria = (TextView) findViewById(R.id.nombreCat);
        nombreCategoria.setText(categorias.getDescripcion());

        retrofitInit();
        generateList();

        Log.d("[PromosPorCategorias]", categorias.getDescripcion());


    }

    private void retrofitInit() {
        misionesClient = MisionesClient.getInstance();
        misionesServices = misionesClient.getMisionesServices();
    }

    private void generateList() {

        String categoria = categorias.getDescripcion();
        Call<List<Promos>> call = misionesServices.getPromosCategorizadas(categoria);

        call.enqueue(new Callback<List<Promos>>() {
            @Override
            public void onResponse(Call<List<Promos>> call, Response<List<Promos>> response) {
                if (response.isSuccessful()) {
                    promocionesList = response.body();
                    promoAdaptador = new PromoAdaptador(PromosPorCategoria.this, promocionesList);
                    recyclerViewPromos.setAdapter(promoAdaptador);
                    Log.d("[PromosPorCategorias]", promocionesList.toString());

                } else {
                    Toast.makeText(PromosPorCategoria.this, "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Promos>> call, Throwable t) {
                Toast.makeText(PromosPorCategoria.this, "Error de conexion", Toast.LENGTH_SHORT).show();


            }
        });

        recyclerViewPromos = findViewById(R.id.promosGrid);
        recyclerViewPromos.setHasFixedSize(true);
        int numberOfColumns = 2;
        recyclerViewPromos.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
    }

    public void volverHome(View view) {
        Intent intent = new Intent(this, home.class);
        startActivity(intent);
    }
}