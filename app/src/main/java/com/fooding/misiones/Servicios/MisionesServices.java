package com.fooding.misiones.Servicios;

import android.annotation.SuppressLint;

import com.fooding.misiones.Model.Carrito;
import com.fooding.misiones.Model.Categorias;
import com.fooding.misiones.Model.Promos;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import com.fooding.misiones.Common.Constantes;
public interface MisionesServices {




        @GET("promos_activas")
        Call<List<Promos>> getPromosActivas();

        // Trae solo productos
        @GET("productos")
        Call<List<Promos>> getProductosActivos();

        @GET("categorias")
        Call<List<Categorias>> getCategorias();

        @GET("promos/categoria?")
        Call<List<Promos>> getPromosCategorizadas(@Query("categoria") String nombreCategoriaId);

        @POST("pedido")
        Call<Carrito> saveCarrito(@Header("temp") String hash, @Body Carrito carrito);





}
