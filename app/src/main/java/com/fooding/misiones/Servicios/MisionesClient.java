package com.fooding.misiones.Servicios;

import com.fooding.misiones.Common.Constantes;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MisionesClient {
private static MisionesClient instance = null;
private MisionesServices misionesServices;
private Retrofit retrofit;

public MisionesClient() {

    retrofit = new Retrofit.Builder()
            .baseUrl(Constantes.API_MISIONES_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    misionesServices = retrofit.create(MisionesServices.class);
}

// Patron Singleton
public static MisionesClient getInstance() {
    if(instance == null) {
        instance = new MisionesClient();
    }
    return instance;
}

public MisionesServices getMisionesServices() {
    return misionesServices;
}

}
