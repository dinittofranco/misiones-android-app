package com.fooding.misiones.RoomDB;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.List;

@Entity(tableName = "carrito_data")
public class TablaCarrito implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int elementId;

    //Crate text column
    @ColumnInfo(name = "promocion")
    private String objectId;
    private String nombre_promo;
    private String imgPromo;
    private int valor;
    private int cantidad;
    private int precioIndividual;

    //Getters y Setters

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public int getPrecioIndividual() {
        return precioIndividual;
    }

    public void setPrecioIndividual(int precioIndividual) {
        this.precioIndividual = precioIndividual;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getElementId() {
        return elementId;
    }

    public void setElementId(int elementId) {
        this.elementId = elementId;
    }

    public String getNombre_promo() {
        return nombre_promo;
    }

    public void setNombre_promo(String nombre_promo) {
        this.nombre_promo = nombre_promo;
    }

    public String getImgPromo() {
        return imgPromo;
    }

    public void setImgPromo(String imgPromo) {
        this.imgPromo = imgPromo;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }


}
