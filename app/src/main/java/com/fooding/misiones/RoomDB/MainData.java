package com.fooding.misiones.RoomDB;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

//Defino el nombre de la tabla
@Entity(tableName = "info_user")
public class MainData implements Serializable {
    //Crate id column
    @PrimaryKey(autoGenerate = true)
    private int  id;

    //Crate text column
    @ColumnInfo(name = "user")
    private String nombre;
    private String direccion;
    private String email;
    private String idGoogle;

    public String getIdGoogle() {
        return idGoogle;
    }

    public void setIdGoogle(String idGoogle) {
        this.idGoogle = idGoogle;
    }

    //Generate Getter and Setter
public String getEmail() {
    return email;
}

    public void setEmail(String email) {
        this.email = email;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
