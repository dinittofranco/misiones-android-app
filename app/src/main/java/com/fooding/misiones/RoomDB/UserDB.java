package com.fooding.misiones.RoomDB;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

// add database entities
@Database(entities = {MainData.class, TablaCarrito.class}, version = 15, exportSchema = false)
public abstract class UserDB extends RoomDatabase {

    private static UserDB database;

    private static String DATABASE_NAME = "database";

    public synchronized static UserDB getInstance(Context context){
        if(database == null){
            database = Room.databaseBuilder(context.getApplicationContext(), UserDB.class, DATABASE_NAME).allowMainThreadQueries().fallbackToDestructiveMigration().build();

        }
        return database;
    }

    public abstract UserDao userDao();
}
