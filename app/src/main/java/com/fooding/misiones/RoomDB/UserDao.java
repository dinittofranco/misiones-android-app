package com.fooding.misiones.RoomDB;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {

//Main data
    //Insert Query
    @Insert(onConflict = REPLACE)
    void insertUsuario(MainData mainData);

    //Update Query
    @Query("UPDATE info_user SET direccion = :sDireccion WHERE id = :sId")
    void updateDireccion(int sId, String sDireccion);

    //Delete all DB
    @Query("DELETE FROM info_user WHERE idGoogle LIKE :idGoo")
    void resetUsuario(String idGoo);

    //Get all dataQuery
    @Query("SELECT * FROM info_user")
    List<MainData> getAllUsuarios();

    //Get user email
    @Query("SELECT * FROM info_user WHERE email LIKE :userEmail ")
    List<MainData> getUserEmail(String userEmail);

    //Get user direccion
    @Query("SELECT * FROM info_user WHERE direccion LIKE :userDireccion ")
    List<MainData> getUserDireccion(String userDireccion);


    //Tabla Carrito
    //Insert Query
    @Insert(onConflict = REPLACE)
    void insertCarrito(TablaCarrito tablaCarrito);

    //Update Query
    @Query("UPDATE carrito_data SET cantidad = :sCantidad WHERE elementId = :sId")
    void updateCarrito(int sId, int sCantidad);

    //Delete all DB
    @Query("DELETE FROM carrito_data")
    void resetCarrito();

    //Delete an unique element
    @Query("DELETE FROM carrito_data WHERE elementId = :pId")
    void deleteElementById(int pId);

    //Get all dataQuery
    @Query("SELECT * FROM carrito_data")
    List<TablaCarrito> getAllPromos();

}
