package com.fooding.misiones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityOptionsCompat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class loginPage extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText email;
    private EditText password;
    private Button iniciarSesion, google, facebook, login_button;
    private ProgressDialog progressDialog;
    private CallbackManager callbackManager = CallbackManager.Factory.create();
    List<String> permisoNecesario = Arrays.asList("email", "user_birthday", "user_friends", "public_profile");
    private CoordinatorLayout coordinatorLayout;
    GoogleSignInClient mGoogleSignInClient;
    private final static int RC_SIGN_IN = 123;



    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            Log.d("[loginPage]", currentUser.toString());
            Intent intent = new Intent(getApplicationContext(), home.class);
            startActivity(intent);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser ();


        email = (EditText) findViewById(R.id.confirmarDireccion);
        password = (EditText) findViewById(R.id.contrasena);
        iniciarSesion = (Button) findViewById(R.id.btnDireccion);
         google = (Button) findViewById(R.id.google);
        /* facebook = (Button) findViewById(R.id.facebook);
        login_button = (Button) findViewById(R.id.login_button);

         */
        progressDialog = new ProgressDialog (this, R.style.Progres);

        // =================================
        // Google
        // =================================
            createRequest();

            google.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signIn();
                }
            });


        // =================================
        // Google
        // =================================

        // =================================
        // Email y contrasena
        // =================================
            iniciarSesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String emailAuth = email.getText().toString();
                    String passAuth = password.getText().toString();



                    if(!passAuth.isEmpty() && !emailAuth.isEmpty()) {
                        iniciarSesion(emailAuth, passAuth);
                        progressDialog.setMessage("Iniciando sesion");
                        progressDialog.show();

                    } else if(emailAuth.isEmpty() && passAuth.isEmpty()) {
                        AlertDialog.Builder campos = new AlertDialog.Builder ( loginPage.this );
                        campos.setMessage ("Los campos deben estar completos");
                        campos.show ();
                    } else if (emailAuth.isEmpty()){
                        email.setError("Debe ingresar un email");
                    } else if(passAuth.isEmpty()) {
                        password.setError("Debe ingresar una contrasena");
                    }


                }
            });

        // =================================
        // Email y contrasena
        // =================================
    }


    // =================================
    // Google
    // =================================
    private void createRequest() {

        // Genero el PopUp de consulta
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Obtengo la instancia GoogleSignInClient
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d("[loginPage]", "firebaseAuthWithGoogle:" + account.getId());
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.d("LOGIN", String.valueOf(e));
                //Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {

        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent intent = new Intent(getApplicationContext(), home.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(loginPage.this, "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }
    // =================================
    // Google
    // =================================


    // =================================
    // Email y contrasena
    // =================================
        private void iniciarSesion(String emailAuth, String passAuth) {

            mAuth.signInWithEmailAndPassword(emailAuth, passAuth)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                progressDialog.dismiss();

                                Log.d("loginPage", "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                Intent boto = new Intent ( loginPage.this , home.class );
                                startActivity ( boto );
                            } else {
                                progressDialog.dismiss();

                                String errorCode = ((FirebaseAuthException) Objects.requireNonNull(task.getException())).getErrorCode();

                                switch (errorCode) {
                                    case "ERROR_INVALID_EMAIL":
                                        email.setError("Ingrese un email valido");
                                        break;
                                    case "ERROR_WRONG_PASSWORD":
                                    case "ERROR_USER_NOT_FOUND":
                                        Toast.makeText(loginPage.this, "Email o contrasena erroneos", Toast.LENGTH_LONG).show();
                                        break;
                                }

                                Log.d("[loginPage]", errorCode);
                            }
                        }
                    });
        }
    // =================================
    // Email y contrasena
    // =================================

    private void updateUI(FirebaseUser user) {
    }


    public void registrarse(View view) {
        Intent registrarse = new Intent(this, signUp.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Slide slide = new Slide(Gravity.LEFT);
                getWindow().setExitTransition(slide);
            startActivity(registrarse, ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, "").toBundle());
        } else {
            startActivity(registrarse);
        }

    }

    //TODO: Programar cambio de contrasena
    public void olvidar_contrasena(View view) {
        Intent olvidar_contrasena = new Intent(this, olvidarContrasena.class);
        startActivity(olvidar_contrasena);
    }

}