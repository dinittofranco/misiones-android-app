package com.fooding.misiones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fooding.misiones.RoomDB.UserDB;
import com.fooding.misiones.RoomDB.MainData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class signUp extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText email, passwordValidate, password, nombre, direccion;
    private Button crearCuenta;
    private ProgressDialog progressDialog;

    List<MainData> dataList = new ArrayList<>();
    UserDB database;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);
        mAuth = FirebaseAuth.getInstance();

        nombre = (EditText) findViewById(R.id.nombre);
        direccion = (EditText) findViewById(R.id.direccion);
        email = (EditText) findViewById(R.id.confirmarDireccion);
        password = (EditText) findViewById(R.id.contrasena);
        passwordValidate = (EditText) findViewById(R.id.contrasenaValidate);
        crearCuenta = (Button) findViewById(R.id.textoPago);
        progressDialog = new ProgressDialog (this, R.style.Progres);

        database = UserDB.getInstance(this);

        dataList = database.userDao().getAllUsuarios();


        crearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailAuth = email.getText().toString();
                String passAuth = password.getText().toString();
                String passValidate = passwordValidate.getText().toString();
                String nombreUsuario = nombre.getText().toString();
                String direccionUsuario = direccion.getText().toString();

                MainData data = new MainData();

                data.setNombre(nombreUsuario);
                data.setDireccion(direccionUsuario);
                data.setEmail(emailAuth);
                database.userDao().insertUsuario(data);

                if(!emailAuth.isEmpty() && !passAuth.isEmpty()){
                       crearUsuario(emailAuth, passAuth, passValidate);
                        progressDialog.setMessage("Iniciando sesion");
                        progressDialog.show();

                    } else {
                    AlertDialog.Builder campos = new AlertDialog.Builder ( signUp.this );
                    campos.setMessage ("Los campos deben estar completos");
                    campos.show ();
                    }

            }
        });
    }


    private Boolean crearUsuario(String emailAuth, String passAuth, String passValidate) {

        if (!passAuth.equals(passValidate)) {
            Toast.makeText(this, "La contrasenas no coinciden", Toast.LENGTH_SHORT).show();
            progressDialog.dismiss();

            return false;
        }

        mAuth.createUserWithEmailAndPassword(emailAuth, passAuth)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("[signUp]", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d("[signUp]", user.toString());

                            navegarHome();

                        } else {
                            progressDialog.dismiss();

                            String errorCode = ((FirebaseAuthException) Objects.requireNonNull(task.getException())).getErrorCode();

                            switch (errorCode) {
                                case "ERROR_EMAIL_ALREADY_IN_USE":
                                    email.setError ("El email ya existe en la base de datos, por favor ingrese uno distinto.");
                                    break;
                                case "ERROR_INVALID_EMAIL":
                                    email.setError ("Ingrese un email valido");
                                    break;
                                case "ERROR_WEAK_PASSWORD":
                                    password.setError("La contrasena debe tener mas de 6 caracteres");
                                    break;
                                case "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                                    Toast.makeText(signUp.this, "El email ingresado corresponde a una cuenta activa. Si se registro con Google o Facebook, por favor inicie sesion con el metodo utilizado", Toast.LENGTH_LONG).show();

                            }

                            Log.w("[signUp]", "createUserWithEmail:failure", task.getException());
                            //Toast.makeText(signUp.this, "Error al crear el usuario, corrobore que este conectado a internet", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        return true;
    }

    public void navegarHome() {
        Intent navegar = new Intent(signUp.this, home.class);
            startActivity(navegar);
    }


    public void ingresar(View view) {
        Intent ingresar = new Intent(signUp.this, loginPage.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Slide slide = new Slide(Gravity.RIGHT);
            getWindow().setExitTransition(slide);
            startActivity(ingresar, ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, "").toBundle());
        } else {
            startActivity(ingresar);
        }
    }
}