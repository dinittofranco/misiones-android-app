package com.fooding.misiones.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Carrito {

    @SerializedName("direccion")
    @Expose
    private String direccion;
    @SerializedName("fecha")
    @Expose
    private String fecha;
    @SerializedName("valor")
    @Expose
    private int valor;
    @SerializedName("pago")
    @Expose
    private Boolean pago;
    @SerializedName("estado")
    @Expose
    private String estado;
    @SerializedName("promociones")
    @Expose
    private List<String> promociones;
    @SerializedName("medio_pago")
    @Expose
    private String medio_pago;
    @SerializedName("data_usuario")
    @Expose
    private String data_usuario;

    public String getData_usuario() { 
        return data_usuario;
    }

    public void setData_usuario(String data_usuario) {
        this.data_usuario = data_usuario;
    }

    public String getMedio_pago() {
        return medio_pago;
    }

    public void setMedio_pago(String medio_pago) {
        this.medio_pago = medio_pago;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Boolean getPago() {
        return pago;
    }

    public void setPago(Boolean pago) {
        this.pago = pago;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<String> getPromociones() {
        return promociones;
    }

    public void setPromociones(List<String> promociones) {
        this.promociones = promociones;
    }
}
