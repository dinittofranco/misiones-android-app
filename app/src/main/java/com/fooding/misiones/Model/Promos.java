package com.fooding.misiones.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fooding.misiones.Common.Constantes;

import java.util.ArrayList;
import java.util.List;

public class Promos implements Parcelable {

    private String nombre_promo;
    private String imgPromo;
    private String valor;
    private List<Producto> productos;
    private String categoria;
    private String _id;

    public Promos(String nombre_promo, String imgPromo, String valor, List<Producto> productos, String _id, String categoria) {
        this.nombre_promo = nombre_promo;
        this.imgPromo = imgPromo;
        this.valor = valor;
        this.productos = productos;
        this.categoria = categoria;
        this._id = _id;
    }

    public String getNombre_promo() {
        return nombre_promo;
    }

    public void setNombre_promo(String nombre_promo) {
        this.nombre_promo = nombre_promo;
    }

    public String getImgPromo() {
        String urlImagen = Constantes.URL_IMAGEN + imgPromo;
        return urlImagen;
    }

    public void setImgPromo(String imgPromo) {
        this.imgPromo = imgPromo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<Producto> getProductos() { return productos; }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "Promos{" +
                "nombre_promo='" + nombre_promo + '\'' +
                ", imgPromo='" + imgPromo + '\'' +
                ", valor='" + valor + '\'' +
                ", productos=" + productos +
                ", categoria='" + categoria + '\'' +
                ", _id='" + _id + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nombre_promo);
        dest.writeString(this.imgPromo);
        dest.writeString(this.valor);
        dest.writeList(this.productos);
        dest.writeString(this.categoria);
        dest.writeString(this._id);
    }

    protected Promos(Parcel in) {
        this.nombre_promo = in.readString();
        this.imgPromo = in.readString();
        this.valor = in.readString();
        this.productos = new ArrayList<>();
        in.readList(this.productos, Producto.class.getClassLoader());
        this.categoria = in.readString();
        this._id = in.readString();
    }

    public static final Creator<Promos> CREATOR = new Creator<Promos>() {
        @Override
        public Promos createFromParcel(Parcel source) {
            return new Promos(source);
        }

        @Override
        public Promos[] newArray(int size) {
            return new Promos[size];
        }
    };
}
