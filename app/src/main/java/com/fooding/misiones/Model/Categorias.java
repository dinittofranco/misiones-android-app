package com.fooding.misiones.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Categorias implements Parcelable {
    private String _id;
    private String descripcion;

    public Categorias(String _id, String descripcion) {
        this._id = _id;
        this.descripcion = descripcion;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._id);
        dest.writeString(this.descripcion);
    }

    protected Categorias(Parcel in) {
        this._id = in.readString();
        this.descripcion = in.readString();
    }

    public static final Creator<Categorias> CREATOR = new Creator<Categorias>() {
        @Override
        public Categorias createFromParcel(Parcel source) {
            return new Categorias(source);
        }

        @Override
        public Categorias[] newArray(int size) {
            return new Categorias[size];
        }
    };
}
