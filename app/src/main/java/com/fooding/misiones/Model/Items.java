package com.fooding.misiones.Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class Items implements Parcelable {

    private List<Promos> promosItems;

    public Items(List<Promos> promosItems) {
        this.promosItems = promosItems;
    }

    public List<Promos> getPromosItems() {
        return promosItems;
    }

    public void setPromosItems(List<Promos> promosItems) {
        this.promosItems = promosItems;
    }

    @NotNull
    @Override
    public String toString() {
        return "Items{" +
                "promosItems=" + promosItems +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.promosItems);
    }

    protected Items(Parcel in) {
        in.readList(this.promosItems, Promos.class.getClassLoader());
    }

    public static final Creator<Items> CREATOR = new Creator<Items>() {
        @Override
        public Items createFromParcel(Parcel source) {
            return new Items(source);
        }

        @Override
        public Items[] newArray(int size) {
            return new Items[size];
        }
    };
}
