package com.fooding.misiones.Adaptador;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fooding.misiones.Carrito;
import com.fooding.misiones.R;
import com.fooding.misiones.RoomDB.TablaCarrito;
import com.fooding.misiones.RoomDB.UserDB;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CarritoAdaptador extends  RecyclerView.Adapter<CarritoAdaptador.ViewHolder>{

    private Activity context;
    private List<TablaCarrito> listCarrito;
    private UserDB database;
    List<TablaCarrito> allTablaCarrito = new ArrayList<>();

    public CarritoAdaptador(Activity context, List<TablaCarrito> listCarrito) {
        this.context = context;
        this.listCarrito = listCarrito;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inicializamos la vista
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_carrito, parent, false);
       ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.eliminarPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TablaCarrito tablaCarrito = listCarrito.get(viewHolder.getAdapterPosition());

                int elementId = tablaCarrito.getElementId();

                database.userDao().deleteElementById(elementId);
                Intent intent = new Intent(context, Carrito.class);
                context.startActivity(intent);
                notifyItemChanged( viewHolder.getAdapterPosition() );
                Toast.makeText(context, "Producto eliminado correctamente", Toast.LENGTH_SHORT).show();

            }
        });


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // Inicializamos TablaCarrito
        TablaCarrito tablaCarrito  = listCarrito.get(position);
        // Inicializamos la database
        database = UserDB.getInstance(context);
        // Seteamos el texto y la imagen a los views
        holder.precio.setText("$" + String.valueOf(tablaCarrito.getValor()));
        holder.nombre_promo.setText(tablaCarrito.getNombre_promo());
        String cantidadToString = String.valueOf(tablaCarrito.getCantidad());
        holder.cantidad_numero.setText(cantidadToString);
        holder.total_precio.setText("$" + String.valueOf(tablaCarrito.getPrecioIndividual()));
     //TODO: holder.cantidad_promo.setText(cantidadToString);



        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(tablaCarrito.getImgPromo())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(holder.promo);



    }

    @Override
    public int getItemCount() {
        return listCarrito.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Inicializamos las variables
        TextView precio, nombre_promo, cantidad_numero, total_precio;
        ImageView promo;
        Button eliminarPromo;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            // Asigno las variables
            precio = itemView.findViewById(R.id.precioPromocion);
            nombre_promo = itemView.findViewById(R.id.nombrePromocion);
            eliminarPromo = itemView.findViewById(R.id.eliminarElemento);
            cantidad_numero = itemView.findViewById(R.id.cantidadNumero);
            total_precio = itemView.findViewById(R.id.totalPrecio);
            promo = itemView.findViewById(R.id.fotoPromo);
        }
    }

    //TODO: Este metodo cambia en tiempo real el precio en pantalla

   /* public interface OnQuantityChangeListener {
        void onQuantityChange( int change );
    }

    */

}
