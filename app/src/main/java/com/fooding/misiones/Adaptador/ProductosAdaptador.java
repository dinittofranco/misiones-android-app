package com.fooding.misiones.Adaptador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.fooding.misiones.Model.Producto;

import java.util.ArrayList;

public class ProductosAdaptador extends RecyclerView.Adapter<ProductosAdaptador.ViewHolder> {

    private final Context context;
    private final ArrayList<Producto> productos;

    public ProductosAdaptador(Context context, ArrayList<Producto> productos) {
        this.context = context;
        this.productos = productos;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(com.fooding.misiones.R.layout.template_productos, parent, false);



        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nombreProd.setText(productos.get(position).getNombre());
        holder.descripcionProd.setText(productos.get(position).getDescripcion());
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private TextView nombreProd, descripcionProd;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            nombreProd = mView.findViewById(com.fooding.misiones.R.id.nombreListProducto);
            descripcionProd = mView.findViewById(com.fooding.misiones.R.id.descripcionListProducto);


        }
    }
}

