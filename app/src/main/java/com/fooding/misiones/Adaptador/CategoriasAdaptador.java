package com.fooding.misiones.Adaptador;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.fooding.misiones.Model.Categorias;
import com.fooding.misiones.Model.Producto;
import com.fooding.misiones.Model.Promos;
import com.fooding.misiones.PromosPorCategoria;
import com.fooding.misiones.R;
import com.fooding.misiones.detallesPromo;

import java.util.ArrayList;
import java.util.List;

import static android.os.Build.VERSION_CODES.R;

public class CategoriasAdaptador extends RecyclerView.Adapter<CategoriasAdaptador.ViewHolder> {

    private final Context context;
    private final List<Categorias> categorias;

    public CategoriasAdaptador(Context context, List<Categorias> categorias) {
        this.context = context;
        this.categorias = categorias;

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(com.fooding.misiones.R.layout.template_categorias, parent, false);



        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.categoriaDescripcion.setText(categorias.get(position).getDescripcion());

        holder.promosCategorizadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCategorias = new Intent(context, PromosPorCategoria.class);
                intentCategorias.putExtra(PromosPorCategoria.extra_Categoria, categorias.get(position));
                context.startActivity(intentCategorias);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categorias.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private TextView categoriaDescripcion;
        private Button promosCategorizadas;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            categoriaDescripcion = mView.findViewById(com.fooding.misiones.R.id.nombreCategoria);
            promosCategorizadas = mView.findViewById(com.fooding.misiones.R.id.btnIr);

        }
    }
}

