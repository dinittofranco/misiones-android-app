package com.fooding.misiones.Adaptador;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fooding.misiones.Model.Promos;
import com.fooding.misiones.R;
import com.fooding.misiones.detallesPromo;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ProductosActivosAdaptador extends RecyclerView.Adapter<ProductosActivosAdaptador.CustomViewHolder> {
    private List<Promos> promosActivas;
    private List<Promos> promocionesResponse = new ArrayList<>();
    private Context context;


    public ProductosActivosAdaptador(Context context, List<Promos> promosActivas) {
        this.context = context;
        this.promosActivas = promosActivas;

    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.tarjeta_promo, viewGroup, false);

        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(promosActivas.get(position).getImgPromo())
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(holder.promo);

        String nombrePromo = promosActivas.get(position).getNombre_promo();
        String valorPromo = promosActivas.get(position).getValor();

        holder.nombre_promo.setText(nombrePromo);
        holder.valor_promo.setText("$" + valorPromo);

        // Parseo el intent para entrar a la activity de detalles

        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetalles = new Intent(context, detallesPromo.class);
                intentDetalles.putExtra(detallesPromo.extra_Promo, promosActivas.get(position));
                context.startActivity(intentDetalles);
            }
        });

    }


    @Override
    public int getItemCount() { return promosActivas.size(); }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private TextView nombre_promo, valor_promo, nombre_producto;
        private ImageView promo;
        private Button button;

        public CustomViewHolder(View view) {
            super(view);
            mView = view;

            nombre_promo = mView.findViewById(R.id.nombrePromo);
            valor_promo = mView.findViewById(R.id.precioPromo);
            promo = mView.findViewById(R.id.imgPromo);
            button = mView.findViewById(R.id.entrarPromo);

        }

    }
}
