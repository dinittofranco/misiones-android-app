package com.fooding.misiones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fooding.misiones.Adaptador.CarritoAdaptador;
import com.fooding.misiones.RoomDB.TablaCarrito;
import com.fooding.misiones.RoomDB.UserDB;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.ArrayList;
import java.util.List;

public class Carrito extends AppCompatActivity {

    RecyclerView recyclerView;

    List<TablaCarrito> tablaCarrito = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    UserDB database;
    CarritoAdaptador adapter;
    Handler handler;
    TextView subPrecio, precio_total;
    Button eliminarTodo, volver, continuar;
    private int total = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);

        recyclerView = findViewById(R.id.promos);

        database = UserDB.getInstance(this);
        tablaCarrito = database.userDao().getAllPromos();
        subPrecio = (TextView) findViewById(R.id.subTotalPrecio);
        precio_total = (TextView) findViewById(R.id.precioTotal);
        eliminarTodo = (Button) findViewById(R.id.eliminar);
        volver = (Button) findViewById(R.id.volverHome);
        continuar = (Button) findViewById(R.id.continuarDireccion);

        eliminarTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tablaCarrito.size() == 0) {
                    FancyToast.makeText(getApplicationContext(),"No hay nada que eliminar",FancyToast.LENGTH_LONG,FancyToast.WARNING, R.drawable.baseline_shopping_cart_white_24dp,false).show();
                } else {
                    database.userDao().resetCarrito();
                    Intent intent = new Intent(getBaseContext(), Carrito.class);
                    startActivity(intent);
                    Toast.makeText(Carrito.this,"Productos eliminados correctamente!", Toast.LENGTH_LONG);
                    FancyToast.makeText(getApplicationContext(),"Productos eliminados correctamente!",FancyToast.LENGTH_LONG,FancyToast.SUCCESS, R.drawable.baseline_delete_red_400_24dp,false).show();
                    finish();
                }
            }
        });

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Carrito.this, home.class);
                startActivity(intent);

                finish();
            }
        });

        int totalPrecio =0;
        for (int i = 0; i < tablaCarrito.size(); i++) {
            totalPrecio = tablaCarrito.get(i).getValor() *tablaCarrito.get(i).getCantidad()  + totalPrecio;
        }
        String pesosSubPrecio = "$" + totalPrecio;
        String pesosTotalPrecio = "";
        if (tablaCarrito.size() == 0){
            pesosTotalPrecio = String.valueOf(totalPrecio);
        } else {
            pesosTotalPrecio = String.valueOf(totalPrecio + 50);

        }

        subPrecio.setText(pesosSubPrecio);
        precio_total.setText(pesosTotalPrecio);
        Log.d("SUBPRECIO", String.valueOf(totalPrecio));

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new CarritoAdaptador(Carrito.this, tablaCarrito);
        recyclerView.setAdapter(adapter);

        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tablaCarrito.size() == 0) {
                   FancyToast.makeText(getApplicationContext(),"El carrito está vacio!",FancyToast.LENGTH_LONG,FancyToast.WARNING, R.drawable.baseline_shopping_cart_white_24dp,false).show();
               } else {
                    Intent intent = new Intent(Carrito.this, ConfirmarDireccion.class);
                    startActivity(intent);
                }
            }
        });

    }


}