package com.fooding.misiones;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.fooding.misiones.Model.Carrito;
import com.fooding.misiones.RoomDB.MainData;
import com.fooding.misiones.RoomDB.TablaCarrito;
import com.fooding.misiones.RoomDB.UserDB;
import com.fooding.misiones.Servicios.MisionesClient;
import com.fooding.misiones.Servicios.MisionesServices;
import com.google.firebase.auth.FirebaseAuth;
import com.shashank.sony.fancytoastlib.FancyToast;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MetodoPago extends AppCompatActivity {
    MisionesServices misionesServices;
    MisionesClient misionesClient;
    FirebaseAuth mAuth;
    UserDB database;
    List<MainData> dataEmail = new ArrayList<>();
    List<TablaCarrito> tablaCarrito = new ArrayList<>();
    private Date fecha;

   /* // Método ejecutado al hacer clic en el botón
    public void submit(View view) {

        // Iniciar el checkout de Mercado Pago
        new MercadoPago.StartActivityBuilder()
                .setActivity(this)
                .setPublicKey("TEST-ad365c37-8012-4014-84f5-6c895b3f8e0a")
                .setCheckoutPreferenceId("150216849-ceed1ee4-8ab9-4449-869f-f4a8565d386f")
                .startCheckoutActivity();
    }

    // Espera los resultados del checkout
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MercadoPago.CHECKOUT_REQUEST_CODE) {
            if (resultCode == RESULT_OK && data != null) {

                // Listo! El pago ya fue procesado por MP.
                Payment payment = JsonUtil.getInstance()
                        .fromJson(data.getStringExtra("payment"), Payment.class);
                TextView results = (TextView) findViewById(R.id.mp_results);

                if (payment != null) {
                    results.setText("PaymentID: " + payment.getId() +
                            " - PaymentStatus: " + payment.getStatus());
                } else {
                    results.setText("El usuario no concretó el pago.");
                }

            } else {
                if ((data != null) && (data.hasExtra("mpException"))) {
                    MPException mpException = JsonUtil.getInstance()
                            .fromJson(data.getStringExtra("mpException"), MPException.class);
                    // Manejá tus errores.
                }
            }
        }
    }

    */



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metodo_pago);
        mAuth = FirebaseAuth.getInstance();
        String currentEmail = mAuth.getCurrentUser().getEmail();

        database = UserDB.getInstance(this);

        dataEmail = database.userDao().getUserEmail(currentEmail);
        tablaCarrito = database.userDao().getAllPromos();

        Button efectivo = (Button) findViewById(R.id.efectivo);

        misionesClient = MisionesClient.getInstance();
        misionesServices = misionesClient.getMisionesServices();

        Calendar calendar = Calendar.getInstance();

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateTime = dateFormat.format(calendar.getTime());
        Log.d("FECHA", dateTime);

        efectivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Carrito carrito = new Carrito();
                carrito.setDireccion(dataEmail.get(0).getDireccion());
                carrito.setEstado("Abierto");
                carrito.setPago(false);
                carrito.setFecha(dateTime);
                carrito.setMedio_pago("Efectivo");
                carrito.setData_usuario(dataEmail.get(0).getNombre());

                int totalPrecio =0;
                List<String> data = new ArrayList<String>();

                for (int i = 0; i < tablaCarrito.size() ; i++) {
                    totalPrecio = tablaCarrito.get(i).getPrecioIndividual() + totalPrecio;
                    for (int j = 0; j < tablaCarrito.get(i).getCantidad(); j++) {
                        data.add(tablaCarrito.get(i).getObjectId());
                        carrito.setPromociones(data);
                    }
                }
                carrito.setValor(totalPrecio);

                String direccion = carrito.getDireccion();
                String estado = carrito.getEstado();
                Boolean pago = carrito.getPago();
                String fecha = carrito.getFecha();
                int valor = carrito.getValor();
                List<String> promociones = carrito.getPromociones();

               Call<Carrito> call = misionesServices.saveCarrito("aedb85b757c2cc2d9136335f45c7c5b4", carrito);
               Log.d("CALL", String.valueOf(carrito.getMedio_pago()));
                call.enqueue(new Callback<Carrito>() {
                    @Override
                    public void onResponse(@NotNull Call<Carrito> call, Response<Carrito> response) {
                        Log.d("CALL", String.valueOf(response));
                        Log.d("CALL", String.valueOf(call));
                        if(response.code() == 400) {
                            FancyToast.makeText(getApplicationContext(),"Ocurrio un error, vuelva a intentar mas tarde",FancyToast.LENGTH_LONG,FancyToast.ERROR, R.drawable.baseline_error_outline_white_24dp,false).show();
                        } else if(response.code() == 200) {
                            FancyToast.makeText(getApplicationContext(),"Pedido efectuado exitosamente!",FancyToast.LENGTH_LONG,FancyToast.SUCCESS, R.drawable.baseline_payments_white_24dp,false).show();
                            database.userDao().resetCarrito();
                            Intent home = new Intent(MetodoPago.this, ConfirmacionPedido.class );
                            startActivity(home);
                        } else {
                            FancyToast.makeText(getApplicationContext(),"Error inesperado, vuelva a intentar!",FancyToast.LENGTH_LONG,FancyToast.ERROR, R.drawable.baseline_error_outline_white_24dp,false).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<Carrito> call, Throwable t) {
                        Log.d("CALL", String.valueOf(t));
                        FancyToast.makeText(getApplicationContext(),"Ocurrio un error, intenta nuevamente!",FancyToast.LENGTH_LONG,FancyToast.ERROR, R.drawable.baseline_payments_white_24dp,false).show();


                    }
                });


            }
        });

    }


}