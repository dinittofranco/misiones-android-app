package com.fooding.misiones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ConfirmacionPedido extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacion_pedido);

        TextView textoCuerpo = (TextView) findViewById(R.id.textoCuerpo);
        TextView textoEncabezado = (TextView) findViewById(R.id.textoEncabezado);
        Button volverInicio = (Button) findViewById(R.id.btnInicio);



    volverInicio.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent volverHome = new Intent(ConfirmacionPedido.this, home.class);
                startActivity(volverHome);
            }
        });
    }
}